# Babylon Health Coding Challenge

As per the requirements, this is a take on a sitemap builder.

Shortcomings due to time constraints are many:

- Some URL building edge cases are not catered to.
- Many anchors that should be ignored are not (such as mailto:) right now.  
- We are not taking any kind of care to avoid circular references (page to itself or an ancestor).
- We are not doing anything useful with the result. We try to print it to console. The circular reference bites us here.
- We are not parallelising anywhere. The downside is slow execution. The inadvertent upside we don't act like we're DOS'ing the target site. 
- Basic test cases are there.
- We are not working as expected. While we interrogate the entire site. The returned result in the end doesn't capture everything.

We do make it a point to only stick within our starting hostname for crawling.

I made it a point to write loosely coupled code (of course) but not use Spring Boot (my normal go to for the last few years).
DI is done at the main() method. We don't have anything complex enough to do IoC.

Spring Boot was used to create the runnable jar.

This requires minimum **Java 9** to run. Runs fine with 11.
Build with 
```
mvn clean package
```
Run with 
```
java -jar BabylonHealthCodingChallenge-1.0-SNAPSHOT-spring-boot.jar [URL]
```
If no URL is provided, the run will default to https://babylonhealth.com .