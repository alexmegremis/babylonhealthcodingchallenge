package com.alexmegremis;

import com.google.gson.Gson;
import org.apache.log4j.BasicConfigurator;

/**
 * Hello world!
 */
public class App {

    static {
        BasicConfigurator.configure();
    }

    public static void main(String[] args) throws Exception {

        UsableURLFinder usableURLFinder = new UsableURLFinderImpl();
        ContentProvider contentProvider = new HttpContentProviderImpl();
        SiteMapper      siteMapper      = new SiteMapperImpl(contentProvider, usableURLFinder);

        SitePage result;
        if(args.length == 0)
            result = siteMapper.createSiteMap("https://babylonhealth.com");
        else {
            result = siteMapper.createSiteMap(args[0]);
        }

        Gson gson = new Gson();
        System.out.println(gson.toJson(result));

        System.out.println("done");
    }
}
