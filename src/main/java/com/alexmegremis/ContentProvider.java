package com.alexmegremis;

import java.io.IOException;
import java.net.URI;

public interface ContentProvider {

    void init(final URI uri) throws IOException;

    Integer getResponseCode();

    String getNextLine() throws IOException;

    void done() throws IOException;
}
