package com.alexmegremis;

import lombok.Getter;
import lombok.Setter;

import java.io.*;
import java.net.*;

public class HttpContentProviderImpl implements ContentProvider {

    BufferedReader in;
    @Getter
    @Setter
    private Integer responseCode;

    @Override
    public void init(final URI uri) throws IOException {
        URL               url = uri.toURL();
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        this.setResponseCode(con.getResponseCode());

        if (this.getResponseCode() != HttpURLConnection.HTTP_NOT_FOUND) {
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        }
    }

    @Override
    public String getNextLine() throws IOException {
        return in.readLine();
    }

    @Override
    public void done() throws IOException {
        in.close();
    }
}
