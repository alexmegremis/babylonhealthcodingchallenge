package com.alexmegremis;

import java.io.IOException;
import java.net.URISyntaxException;

@FunctionalInterface
public interface SiteMapper {

    SitePage createSiteMap(String rootUriAsString) throws IOException, URISyntaxException;
}
