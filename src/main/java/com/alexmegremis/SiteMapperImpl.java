package com.alexmegremis;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.*;
import java.util.*;

@Slf4j
@AllArgsConstructor
public class SiteMapperImpl implements SiteMapper {

    public static final String ROOT_NAME = "ROOT";

    private final Map<URI, SitePage> pagesCrawled = new HashMap<>();
    private       ContentProvider    contentProvider;
    private       UsableURLFinder    urlFinder;

    @Override
    public SitePage createSiteMap(final String rootUriAsString) throws IOException, URISyntaxException {

        SitePage root = new SitePage(ROOT_NAME, rootUriAsString);
        this.doCrawl(root);

        return root;
    }

    /**
     * Recursive function for drilling down through pages and their children.
     *
     * @param sitePage
     *
     * @throws IOException
     * @throws URISyntaxException
     */
    public void doCrawl(final SitePage sitePage) throws IOException, URISyntaxException {

        log.info(">>> Crawling {}", sitePage);

        contentProvider.init(sitePage.getUri());

        sitePage.setResponseCode(contentProvider.getResponseCode());

        if (sitePage.getResponseCode() != HttpURLConnection.HTTP_NOT_FOUND) {
            log.info(">>> {} response : {}", sitePage, sitePage.getResponseCode());

            String         inputLine;
            List<SitePage> usableURLs = new ArrayList<>();

            while ((inputLine = contentProvider.getNextLine()) != null) {
//                log.debug(">>> {} inputline : {}", sitePage, inputLine);
                Optional<List<SitePage>> possibleURLs = urlFinder.findUsableURLs(inputLine, sitePage.getRoot());
                possibleURLs.ifPresent(usableURLs :: addAll);
            }

            contentProvider.done();
            log.info(">>> {} done reading", sitePage);

            // This bit might be extractable
            if (! usableURLs.isEmpty()) {

                // We don't want to read the same page repeatedly
                // also this anonymous inner is bad bad juju, but lazy FTW for now
                usableURLs.stream().filter(e -> ! pagesCrawled.containsKey(e.getUri())).forEach(u -> {
                    pagesCrawled.put(u.getUri(), u);

                    log.info(">>> {} processing {}", sitePage, u.getURIASCIIText());

                    if (u.getUri().getHost().equals(u.getRoot().getUri().getHost())) {
                        try {
                            log.info(">>> {} recursing into {}", sitePage, u.getURIASCIIText());
                            doCrawl(u);
                        } catch (Exception e) {
                            log.error(e.getLocalizedMessage(), e);
                        }
                    }
                });

                log.info(">>> {} adding children", sitePage);
                sitePage.getChildren().addAll(usableURLs);
            }
        }
    }
}
