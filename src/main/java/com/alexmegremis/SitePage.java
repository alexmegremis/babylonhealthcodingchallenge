package com.alexmegremis;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.http.client.utils.URIBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class SitePage {

    final   SitePage root;
    final   String   name;
    final   URI      uri;
    final List<SitePage> children = new ArrayList<>();
    private Integer  responseCode;

    public SitePage(final String name, final String uriAsString) throws URISyntaxException {
        this(name, uriAsString, null);
    }

    public SitePage(final String name, final String uriAsString, final SitePage root) throws URISyntaxException {
        this.root = root == null ? this : root;
        this.name = name;
        this.uri = createFullURI(uriAsString);
    }

    private URI createFullURI(final String uriAsString) throws URISyntaxException {
        URI result = URI.create(uriAsString);
        if (result.getHost() == null) {
            URIBuilder uriBuilder = new URIBuilder(root.getUri());
            uriBuilder.setPath(uriAsString);
            result = uriBuilder.build();
        }
        return result;
    }

    public String getURIASCIIText() {
        return this.getUri().toASCIIString();
    }

    @Override
    public int hashCode() {
        int result = getName().hashCode();
        result = 31 * result + getUri().hashCode();
        return result;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final SitePage sitePage = (SitePage) o;

        if (! getName().equals(sitePage.getName())) {
            return false;
        }
        return getUri().equals(sitePage.getUri());
    }

    @Override
    public String toString() {
        return "SitePage{" + "name='" + name + '\'' + ", uri=" + uri + '}';
    }
}
