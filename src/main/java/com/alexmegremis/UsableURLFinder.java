package com.alexmegremis;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@FunctionalInterface
public interface UsableURLFinder {

    Optional<List<SitePage>> findUsableURLs(final String line, final SitePage root) throws URISyntaxException;
}
