package com.alexmegremis;

import java.net.URISyntaxException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UsableURLFinderImpl implements UsableURLFinder {

    public static final Pattern REGEX                  = Pattern.compile(".*?<a\\s.*?(?:href=)\"(?<uri>.*?)\".*?>(?<name>.*?)</a>.*?");
    public static final String  REGEX_NAMED_GROUP_NAME = "name";
    public static final String  REGEX_NAMED_GROUP_URI  = "uri";

    /**
     * We look at a String as a single line and find 0-N anchor links (see {@link UsableURLFinderImpl#REGEX})
     *
     * @param line
     * @param root
     * @return
     * @throws URISyntaxException
     */
    @Override
    public Optional<List<SitePage>> findUsableURLs(final String line, final SitePage root) throws URISyntaxException {

        Optional<List<SitePage>> result = null;

        Matcher matcher = REGEX.matcher(line);
        if (matcher.matches()) {
            List<SitePage> pages = new ArrayList<>();
            matcher.reset();
            while (matcher.find()) {
                SitePage sitePage = new SitePage(matcher.group(REGEX_NAMED_GROUP_NAME), matcher.group(REGEX_NAMED_GROUP_URI), root);
                pages.add(sitePage);
            }

            result = Optional.of(pages);
        } else {
            result = Optional.empty();
        }

        return result;
    }
}
