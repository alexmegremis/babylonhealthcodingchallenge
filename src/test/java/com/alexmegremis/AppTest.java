package com.alexmegremis;

import org.apache.log4j.BasicConfigurator;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple App.
 */
public class AppTest {

    static {
        BasicConfigurator.resetConfiguration();
        BasicConfigurator.configure();
    }

    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        assertTrue(true);
    }
}
