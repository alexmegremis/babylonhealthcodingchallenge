package com.alexmegremis;

import lombok.extern.slf4j.Slf4j;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@Slf4j
@RunWith (MockitoJUnitRunner.class)
public class SiteMapperImplTest {

    private final SitePage page01       = sitePageFactory("ROOT", "https://01", null);
    private final SitePage page01_01    = sitePageFactory("01_01", "https://01/01", page01);
    private final SitePage page01_01_01 = sitePageFactory("01_01_01", "https://01/01/01", page01);
    private final SitePage page01_01_02 = sitePageFactory("01_01_02", "https://01/01/02", page01);
    private final SitePage page01_02    = sitePageFactory("01_02", "https://01/02", page01);
    private final SitePage page01_03    = sitePageFactory("01_03", "https://01/03", page01);
    private final SitePage page01_03_01 = sitePageFactory("01_03_01", "https://01/03/01", page01);
    private final SitePage page01_03_02 = sitePageFactory("01_03_02", "https://01/03/02", page01);

    @Mock
    UsableURLFinder mockUsableURLFinder;
    @Mock
    ContentProvider mockContentProvider;

    @InjectMocks
    private SiteMapperImpl concreteRef;
    private SiteMapper     ref;

    @Before
    public void setUp() throws Exception {
        ref = concreteRef;

        doReturn(Optional.of(List.of(page01))).when(mockUsableURLFinder).findUsableURLs(createHref(page01), null);
        doReturn(Optional.of(List.of(page01_01))).when(mockUsableURLFinder).findUsableURLs(createHref(page01_01), page01);
        doReturn(Optional.of(List.of(page01_02))).when(mockUsableURLFinder).findUsableURLs(createHref(page01_02), page01);
        doReturn(Optional.of(List.of(page01_03))).when(mockUsableURLFinder).findUsableURLs(createHref(page01_03), page01);
    }

    private String createHref(final SitePage page) {
        String result = "<a href=\"" + page.getURIASCIIText() + "\">" + page.getName() + "</a>";
        log.info(">>> Created anchor link {}", result);
        return result;
    }

    @After
    public void tearDown() throws Exception {
        ref = null;
        concreteRef = null;
//        mockUsableURLFinder = null;
//        mockContentProvider = null;
        Mockito.reset(mockContentProvider);
        Mockito.reset(mockUsableURLFinder);
    }

    @Test
    public void createSiteMap_simple() throws IOException, URISyntaxException {

        doReturn(createHref(page01_01), createHref(page01_02), createHref(page01_03), null).when(mockContentProvider).getNextLine();

        doReturn(200, 200, 200, 200).when(mockContentProvider).getResponseCode();

        concreteRef = new SiteMapperImpl(mockContentProvider, mockUsableURLFinder);

        SitePage actual = concreteRef.createSiteMap(page01.getUri().toASCIIString());

        verify(mockContentProvider).init(page01.getUri());
        verify(mockContentProvider).init(page01_01.getUri());
        verify(mockContentProvider).init(page01_02.getUri());
        verify(mockContentProvider).init(page01_03.getUri());

        verify(mockUsableURLFinder).findUsableURLs(createHref(page01_01), page01);
        verify(mockUsableURLFinder).findUsableURLs(createHref(page01_02), page01);
        verify(mockUsableURLFinder).findUsableURLs(createHref(page01_03), page01);

        verify(mockContentProvider, times(4)).init(any(URI.class));
        verify(mockContentProvider, times(4)).done();
        verify(mockContentProvider, times(4)).getResponseCode();
        verify(mockContentProvider, times(7)).getNextLine();

        verifyNoMoreInteractions(mockUsableURLFinder);
        verifyNoMoreInteractions(mockContentProvider);

        assertFalse(actual.getChildren().isEmpty());
        assertTrue(actual.getChildren().size() == 3);
        actual.getChildren().stream().forEach(e -> assertTrue(e.getChildren().isEmpty()));
    }

    @Test
    public void createSiteMap_complex() throws IOException, URISyntaxException {

        doReturn(Optional.of(List.of(page01_01_01))).when(mockUsableURLFinder).findUsableURLs(createHref(page01_01_01), page01);
        doReturn(Optional.of(List.of(page01_01_02))).when(mockUsableURLFinder).findUsableURLs(createHref(page01_01_02), page01);
        doReturn(Optional.of(List.of(page01_03_01))).when(mockUsableURLFinder).findUsableURLs(createHref(page01_03_01), page01);
        doReturn(Optional.of(List.of(page01_03_02))).when(mockUsableURLFinder).findUsableURLs(createHref(page01_03_02), page01);

        doReturn(createHref(page01_01), createHref(page01_02), createHref(page01_03), null,
                 createHref(page01_01_01), createHref(page01_01_02), null, //end of children for 01_01
                 null, // no children for 01_01_01
                 null, // no children for 01_01_02
                 null, // no children for 01_02
                 createHref(page01_03_01), createHref(page01_03_02), null, //end of children for 01_03
                 null, // no children for 01_03_01
                 null  // no children for 01_03_01
        ).when(mockContentProvider).getNextLine();

        doReturn(200, 200, 200, 200, 200, 200, 200, 200).when(mockContentProvider).getResponseCode();

        concreteRef = new SiteMapperImpl(mockContentProvider, mockUsableURLFinder);

        SitePage actual = concreteRef.createSiteMap(page01.getUri().toASCIIString());

        verify(mockContentProvider).init(page01.getUri());
        verify(mockContentProvider).init(page01_01.getUri());
        verify(mockContentProvider).init(page01_02.getUri());
        verify(mockContentProvider).init(page01_03.getUri());
        verify(mockContentProvider).init(page01_01_01.getUri());
        verify(mockContentProvider).init(page01_01_02.getUri());
        verify(mockContentProvider).init(page01_03_01.getUri());
        verify(mockContentProvider).init(page01_03_02.getUri());

        verify(mockUsableURLFinder).findUsableURLs(createHref(page01_01), page01);
        verify(mockUsableURLFinder).findUsableURLs(createHref(page01_02), page01);
        verify(mockUsableURLFinder).findUsableURLs(createHref(page01_03), page01);
        verify(mockUsableURLFinder).findUsableURLs(createHref(page01_01_01), page01);
        verify(mockUsableURLFinder).findUsableURLs(createHref(page01_01_02), page01);
        verify(mockUsableURLFinder).findUsableURLs(createHref(page01_03_01), page01);
        verify(mockUsableURLFinder).findUsableURLs(createHref(page01_03_02), page01);

        verify(mockContentProvider, times(8)).init(any(URI.class));
        verify(mockContentProvider, times(8)).done();
        verify(mockContentProvider, times(8)).getResponseCode();
        verify(mockContentProvider, times(15)).getNextLine();

        assertFalse(actual.getChildren().isEmpty());
        assertEquals(3, actual.getChildren().size());
        assertTrue(actual.getChildren().stream().anyMatch(s -> s.equals(page01_01)));
        assertTrue(actual.getChildren().stream().anyMatch(s -> s.equals(page01_02)));
        assertTrue(actual.getChildren().stream().anyMatch(s -> s.equals(page01_03)));

        SitePage actual01_01 = actual.getChildren().stream().filter(s -> s.equals(page01_01)).findAny().get();
        SitePage actual01_02 = actual.getChildren().stream().filter(s -> s.equals(page01_02)).findAny().get();
        SitePage actual01_03 = actual.getChildren().stream().filter(s -> s.equals(page01_03)).findAny().get();

        assertFalse(actual01_01.getChildren().isEmpty());
        assertTrue(actual01_02.getChildren().isEmpty());
        assertFalse(actual01_03.getChildren().isEmpty());

        assertEquals(2, actual01_01.getChildren().size());
        assertTrue(actual01_01.getChildren().contains(page01_01_01));
        assertTrue(actual01_01.getChildren().contains(page01_01_02));
        assertEquals(2, actual01_03.getChildren().size());
        assertTrue(actual01_03.getChildren().contains(page01_03_01));
        assertTrue(actual01_03.getChildren().contains(page01_03_02));
    }

    private SitePage sitePageFactory(final String name, final String uriAsString, final SitePage root) {
        SitePage result;
        try {
            result = new SitePage(name, uriAsString, root);
        } catch (URISyntaxException e) {
            result = null;
        }

        log.info(">>> Created SitePage {}", result);
//        System.out.println(result);

        return result;
    }
}