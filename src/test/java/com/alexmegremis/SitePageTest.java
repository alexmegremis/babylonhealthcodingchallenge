package com.alexmegremis;

import org.junit.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SitePageTest {

    public static final String NAME_DOWNLOADS          = "Downloads";
    public static final String NAME_ROOT               = "ROOT";
    public static final String URI_PARTIAL_DOWNLOADS   = "/downloads";
    public static final String URL_DOWNLOADS           = "https://babylonhealth.com/downloads";
    public static final String URL_ROOT_CLEAN          = "https://babylonhealth.com";
    public static final String URL_ROOT_TRAILING_SLASH = "https://babylonhealth.com/";
    private SitePage rootClean;
    private SitePage rootDirty;

    @Before
    public void setUp() throws Exception {
        rootClean = new SitePage(NAME_ROOT, URL_ROOT_CLEAN);
        rootDirty = new SitePage(NAME_ROOT, URL_ROOT_TRAILING_SLASH);
    }

    @After
    public void tearDown() throws Exception {
        rootClean = null;
        rootDirty = null;
    }

    @Test
    public void createRoot() throws Exception {
        SitePage test = new SitePage(NAME_ROOT, URL_ROOT_CLEAN);
        assertNotNull(test.getName());
        assertEquals(NAME_ROOT, test.getName());
        assertNotNull(test.getUri());
        assertEquals(URL_ROOT_CLEAN, test.getUri().toASCIIString());
    }

    @Test
    public void createFromFull_buildFullURIFromCleanRoot() throws Exception {
        SitePage test = new SitePage(NAME_DOWNLOADS, URL_DOWNLOADS, rootClean);
        assertNotNull(test.getName());
        assertEquals(NAME_DOWNLOADS, test.getName());
        assertNotNull(test.getUri());
        assertEquals(URL_DOWNLOADS, test.getUri().toASCIIString());
    }

    @Test
    public void createFromPartial_buildFullURIFromCleanRoot() throws Exception {
        SitePage test = new SitePage(NAME_DOWNLOADS, URI_PARTIAL_DOWNLOADS, rootClean);
        assertNotNull(test.getName());
        assertEquals(NAME_DOWNLOADS, test.getName());
        assertNotNull(test.getUri());
        assertEquals(URL_DOWNLOADS, test.getUri().toASCIIString());
    }

    @Test
    public void createFromPartial_buildFullURIFromDirtyRoot() throws Exception {
        SitePage test = new SitePage(NAME_DOWNLOADS, URI_PARTIAL_DOWNLOADS, rootDirty);
        assertNotNull(test.getName());
        assertEquals(NAME_DOWNLOADS, test.getName());
        assertNotNull(test.getUri());
        assertEquals(URL_DOWNLOADS, test.getUri().toASCIIString());
    }
}