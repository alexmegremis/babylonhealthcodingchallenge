package com.alexmegremis;

import org.junit.*;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class UsableURLFinderImplTest {

    public static final String HTML_MULTIPLE_COMPLEX_REGISTER_LOGIN = "some stuff <a class=\"navigation__list__item__link navigation__list__item__link--cta\" href=\"https://online.babylonhealth.com/register\" foo=\"bar\">Sign up</a>   <a class=\"navigation__list__item__link navigation__list__item__link--cta\" href=\"https://online.babylonhealth.com/sign-in\">Log in</a> foo bar";
    public static final String HTML_NO_MATCH                        = "These aren't the droids you're looking for.";
    public static final String HTML_SINGLE_COMPLEX_DOWNLOAD         = "some stuff <a class=\"button button--outline button--nav\" href=\"/download-app\">Download App</a> foo bar";
    public static final String HTML_SINGLE_SIMPLE_DOWNLOAD          = "some stuff <a href=\"/download-app\">Download App</a> some other stuff";
    public static final String URL_DOWNLOAD                         = "https://online.babylonhealth.com/download-app";
    public static final String URL_LOG_IN                           = "https://online.babylonhealth.com/sign-in";
    public static final String URL_REGISTER                         = "https://online.babylonhealth.com/register";
    public static final String URL_ROOT                             = "https://online.babylonhealth.com";

    private UsableURLFinderImpl concreteRef;
    private SitePage            pageDownload;
    private SitePage            pageLogIn;
    private SitePage            pageSignUp;
    private UsableURLFinder     ref;
    private SitePage            root;

    @Before
    public void setUp() throws Exception {
        root = new SitePage("Babylon Health", URL_ROOT);
        pageSignUp = new SitePage("Sign up", URL_REGISTER);
        pageLogIn = new SitePage("Log in", URL_LOG_IN);
        pageDownload = new SitePage("Download App", URL_DOWNLOAD);

        concreteRef = new UsableURLFinderImpl();
        ref = concreteRef;
    }

    @After
    public void tearDown() throws Exception {
        ref = null;
        concreteRef = null;
    }

    @Test
    public void findUsableURLs_findMultipleComplex() throws Exception {
        List<SitePage>           expected = List.of(pageSignUp, pageLogIn);
        Optional<List<SitePage>> result   = ref.findUsableURLs(HTML_MULTIPLE_COMPLEX_REGISTER_LOGIN, root);

        assertTrue(result.isPresent());
        assertEquals(expected.size(), result.get().size());
        assertEquals(expected.get(0), result.get().get(0));
        assertEquals(expected.get(1), result.get().get(1));
    }

    @Test
    public void findUsableURLs_findSingleComplex() throws Exception {
        List<SitePage>           expected = List.of(pageDownload);
        Optional<List<SitePage>> result   = ref.findUsableURLs(HTML_SINGLE_COMPLEX_DOWNLOAD, root);

        assertTrue(result.isPresent());
        assertEquals(expected.size(), result.get().size());
        assertEquals(expected.get(0), result.get().get(0));
    }

    @Test
    public void findUsableURLs_findSingleSimple() throws Exception {
        List<SitePage>           expected = List.of(pageDownload);
        Optional<List<SitePage>> result   = ref.findUsableURLs(HTML_SINGLE_SIMPLE_DOWNLOAD, root);

        assertTrue(result.isPresent());
        assertEquals(expected.size(), result.get().size());
        assertEquals(expected.get(0), result.get().get(0));
    }

    @Test
    public void findUsableURLs_noMatch() throws Exception {
        Optional<List<SitePage>> result = ref.findUsableURLs(HTML_NO_MATCH, root);

        assertFalse(result.isPresent());
    }
}